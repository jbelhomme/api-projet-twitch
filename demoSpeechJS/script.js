var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent;

let scene = ["discussion", "jeu", "programmation"];
let action = ["mettre scène", "changer scène", "passer scène", "monte le son", "baisse le son"];
let grammar = "#JSGF V1.0; grammar colorFR; public <action> = "+action.join(" | ")+";";

let recognition = new SpeechRecognition();
//Set grammar
let speechRecognitionList = new SpeechGrammarList();
speechRecognitionList.addFromString(grammar, 1);
recognition.grammars = speechRecognitionList;
//Set recognition parameters
recognition.continuous = true;
recognition.lang = 'fr-FR';
recognition.interimResults = false;
recognition.maxAlternatives = 1;

document.getElementById("active").onclick = function() {
    recognition.start();
    console.log("talk...");
}

document.getElementById("desactive").onclick = function() {
    recognition.stop();
    console.log("stop");
}

recognition.onresult = function(event) {
    let result = event.results[event.results.length-1][0].transcript;
    let find = false;
    scene_grammar.forEach(speech => {
        if(result.match(speech)) {
            console.log("-"+result.match(/scène\:? ([A-Za-z]+ ?)\ /)[1]+"-");
            find = true;
        } else if(result.match("monte le son")) {
            //son++
            find = true;
        } else if(result.match("baiss le son")) {
            //son--;
            find = true;
        }
    });

    document.getElementById("span").textContent = result;
    console.log(find == true ? "action comprise" : "action non comprise");
    //console.log(result);
}

recognition.onerror = function(event) {
    //show error
}
