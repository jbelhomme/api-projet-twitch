var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent;

let colorCSS = ["red", "green", "blue"];
let colorFR = ["rouge", "vert", "bleu"];
let grammar = "#JSGF V1.0; grammar colorFR; public <color> = "+colorFR.join(" | ")+";";

let recognition = new SpeechRecognition();
//Set grammar
let speechRecognitionList = new SpeechGrammarList();
speechRecognitionList.addFromString(grammar, 1);
recognition.grammars = speechRecognitionList;
//Set recognition parameters
recognition.continuous = true;
recognition.lang = 'fr-FR';
recognition.interimResults = false;
recognition.maxAlternatives = 1;

document.getElementById("active").onclick = function() {
    recognition.start();
    console.log("talk...");
}

document.getElementById("desactive").onclick = function() {
    recognition.stop();
    console.log("stop");
}


var synth = window.speechSynthesis;
var voices = [];


recognition.onresult = function(event) {
    let result = event.results[event.results.length-1][0].transcript;
    colorFR.forEach(color => { 
        if(result.match(color)) {
            document.getElementsByTagName("body")[0].style.backgroundColor = colorCSS[colorFR.indexOf(color)];
        } 
    });

    if(result.match("Salut")) {
        var utterThis = new SpeechSynthesisUtterance("Bonjour monsieur.");
        utterThis.pitch = 0;
        utterThis.voice = synth.getVoices()[10];
        synth.speak(utterThis);
    } else if(result.match("comment ça va")) {
        var utterThis = new SpeechSynthesisUtterance("Très bien merci, et vous monsieur ?");
        utterThis.pitch = 0;
        utterThis.voice = synth.getVoices()[10];
        synth.speak(utterThis);
    }

    document.getElementById("span").textContent = result;
    console.log(result);
}

recognition.onnomatch = function(event) {
    //show no match
}

recognition.onerror = function(event) {
    //show error
}
