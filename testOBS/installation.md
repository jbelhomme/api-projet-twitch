- Installer OBS  
- Installer package obs-websocket  
[Télécharger le package](https://github.com/palakis/obs-websocket/releases)
- Au lancement d'OBS, renseigner le port (4444 conseillé); et dans notre cas de test, supprimer le mot de passe  
- Dans le code JS: 
```javascript 
obs.connect({
        address: 'localhost:4444',
        password: '[à renseigner]'
    })
    .then(() => {
        console.log(`Success! We're connected & authenticated.`);
 ```


