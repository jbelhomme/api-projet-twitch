const sound = require("sound-play");
const path = require("path");

function playSound(soundFilename, volume){
    const filePath = path.join(__dirname, "/audio/" + soundFilename);

    try {
        sound.play(filePath, volume);
        console.log("Sound success : " + filePath);
    } catch (error) {
        console.error(error);
    }
}

const file = "bruh.mp3";

playSound(file, 0.1);
